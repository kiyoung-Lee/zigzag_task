import React from 'react';

export default class Form extends React.Component{
    state = {
        name: '',
        description: '',
        price: null,
        saleprice: null,
        image1: '',
        imagePreviewUrl1: '',
        image2: '',
        imagePreviewUrl2: '',
        image3: '',
        imagePreviewUrl3: ''
    };

    change = e => {
        this.setState({
            [e.target.name]: e.target.value
        });        
    };

    onSubmit = e => {
        e.preventDefault();
        if(this.formValidate()){
            this.props.onSubmit(this.state);        
            this.stateReset();
            this.elementValueReset();
            alert("아이템 등록 성공!");
        }        
    };

    stateReset = () =>{
        this.setState({
            name: '',
            description: '',
            price: null,
            saleprice: null,
            image1: '',
            imagePreviewUrl1:'',
            image2: '',
            imagePreviewUrl2:'',
            image3: '',        
            imagePreviewUrl3:''
        });    
    };

    elementValueReset = () =>{
        document.getElementById("price").value = "";
        document.getElementById("saleprice").value = "";
        document.getElementById("fileselect1").value = "";
        document.getElementById("fileselect2").value = "";
        document.getElementById("fileselect3").value = "";
    };

    formValidate = () =>{        
        if(this.state.name === ""){
            alert("상품명은 필수 입니다.");
            return false;
        }

        if(this.state.description === ""){
            alert("상품 설명은 필수 입니다.");
            return false;
        }

        if(((this.state.price == null) || (this.state.price < 0))){
            alert("상품가격은 0원 이상입니다.");
            return false;
        }

        if(this.state.saleprice < 0){
            alert("할인가격은 0원 이상입니다.");
            return false;
        }        
        
        if(((this.state.image1 !== '') || (this.state.image2 !== '') || (this.state.image3 !== '')) === false){            
            alert("이미지는 1개이상 필수 등록해야 합니다.");
            return false;            
        }

        return true;
    };   
 
    fileSelect = (e, index) =>{        
        let reader = new FileReader();
        let file = e.target.files[0];       

        reader.onloadend = () =>{
            this.setState({
                ['image' + index]: file,
                ['imagePreviewUrl' + index]: reader.result
            })                   
        };

        reader.readAsDataURL(file);
    };

    render() {        
        return (            
            <form>
                <h1>Item Regist Form</h1>
                <div>
                    <label for="name">상품명(필수)</label>
                    <input 
                    name ="name"                    
                    value={this.state.name} 
                    onChange={e => this.change(e)} 
                    />
                </div>                
                <br />
                <div>
                    <label for="name">상품 설명(필수)</label>
                    <input 
                    name = "description"                    
                    value={this.state.description} 
                    onChange={e => this.change(e)} 
                    />
                </div>                
                <br />
                <div>
                    <label for="name">가격(필수)</label>
                    <input 
                    id = "price"
                    name = "price"                    
                    value={this.state.price} 
                    onChange={e => this.change(e)} 
                    />
                </div>                
                <br />
                <div>
                    <label for="name">할인가격(선택)</label>
                    <input 
                    id = "saleprice"
                    name= "saleprice"                    
                    value={this.state.saleprice} 
                    onChange={e => this.change(e)} 
                    />
                </div>                
                <br />                     
                <div>
                    <h3>상품 이미지 선택(최소 1개)</h3>
                    <table>
                        <tr>
                            <td>
                                <div>
                                    <p>파일 1</p>
                                    <input id="fileselect1" name="image1" type="file" onInput = {(e) => this.fileSelect(e,1)} />
                                    <div>
                                        <img src={this.state.imagePreviewUrl1} height="100" width="100" />
                                    </div>                                    
                                </div>
                            </td>
                            <td>
                                <div>
                                    <p>파일 2</p>
                                    <input id="fileselect2" name="image2" type="file" onChange = {(e) => this.fileSelect(e,2)} />
                                    <div>
                                        <img src={this.state.imagePreviewUrl2} height="100" width="100" />
                                    </div>                                    
                                </div>
                            </td>
                            <td>
                                <div>
                                    <p>파일 3</p>
                                    <input id="fileselect3" name="image3" type="file" onChange = {(e) => this.fileSelect(e,3)} />
                                    <div>
                                        <img src={this.state.imagePreviewUrl3} height="100" width="100" />
                                    </div>                                    
                                </div>
                            </td>
                        </tr>
                    </table>                    
                </div>                
                <div>
                    <button onClick={e => this.onSubmit(e)}> Regist </button>
                </div>                
            </form>
        );
    }
}