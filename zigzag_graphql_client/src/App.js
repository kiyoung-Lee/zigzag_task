import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Form from "./Form";

const uploadfileMutation = gql`
  mutation($name:String, $description: String, $price: Int, $sale_price: Int, $image1: Upload, $image2: Upload, $image3: Upload) {
    uploadFile(name: $name, description: $description, price: $price, sale_price: $sale_price, image1: $image1, image2: $image2, image3: $image3)
  }
`;

export default () => (
  <Mutation mutation={uploadfileMutation}>
    {mutate => (
      <div>        
        <Form onSubmit={fields => {
          const name = fields.name;
          const description = fields.description;
          const price = fields.price;
          const sale_price = fields.saleprice;
          const image1 = fields.image1;      
          const image2 = fields.image2;
          const image3 = fields.image3;                      
          mutate({ variables: { name, description, price, sale_price, image1, image2, image3 }}
        )}}/>        
      </div>           
    )}
  </Mutation>
)
