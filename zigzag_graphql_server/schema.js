export default `
scalar Upload

type Mutation {
    uploadFile(name:String, description:String, price:Int, sale_price:Int, image1:Upload, image2:Upload, image3:Upload): Boolean!
}

type Query{
    hello: String
}
`;