var db = require("./db_repo.js");
var toArray = require('stream-to-array');

var imageBlob1 = null;
var imageBlob2 = null;
var imageBlob3 = null;

var isCallBackSuccuess1 = false;
var isCallBackSuccuess2 = false;
var isCallBackSuccuess3 = false;

const storeUpload = (image, fuc) => {
    const { stream } = image;          
    toArray(stream, fuc);   
};

const variableReset = () =>{
    imageBlob1 = null;
    imageBlob2 = null;
    imageBlob3 = null;
    isCallBackSuccuess1 = false;
    isCallBackSuccuess2 = false;
    isCallBackSuccuess3 = false;
};

export default {
    Query: {
        hello: () => "hi"
    },
    Mutation: {       
        uploadFile: async (parent, args) => {
            const image1 = await args.image1;
            const image2 = await args.image2;
            const image3 = await args.image3;
            
            await storeUpload(image1, function (err, arr) {                     
                if(arr.length != 0){
                    console.log("Image1 Read Success");
                    imageBlob1 = arr.toString(); 
                }                
                isCallBackSuccuess1 = true;
                console.log("Image1 Callback Success");
            });
            
            await storeUpload(image2, function (err, arr) { 
                if(arr.length != 0){
                    console.log("Image2 Read Success");
                    imageBlob2 = arr.toString(); 
                }
                isCallBackSuccuess2 = true;
                console.log("Image2 Callback Success");
            });
            
            await storeUpload(image3, function (err, arr) { 
                if(arr.length != 0){
                    console.log("Image3 Read Success");
                    imageBlob3 = arr.toString(); 
                }
                isCallBackSuccuess3 = true;
                console.log("Image3 Callback Success");
            });         

            // 3rd try
            var i;
            var dbUpload = false;
            for (i = 0; i < 3; i++) { 
                setTimeout(function(){
                    console.log("Callback Count")                    
                        if(isCallBackSuccuess1 == true && isCallBackSuccuess2 == true && isCallBackSuccuess3 == true){     
                            if(dbUpload == false){
                                console.log("File Read Success");               
                                db.upload(args, imageBlob1, imageBlob2, imageBlob3);                                
                                dbUpload = true;
                            }
                        }
                }, 1000);
            }

            variableReset();                        
            return dbUload;
        }
    }
};
