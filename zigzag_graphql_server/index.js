import express from 'express';
import cors from 'cors';
import bodyParser from "body-parser";
import { graphqlExpress } from "graphql-server-express";
import { apolloUploadExpress } from "apollo-upload-server";
import { makeExecutableSchema } from "graphql-tools";

import typeDefs from "./schema";
import resolvers from "./resolvers";

const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});

const app = express();

app.use(
    "/image",
    cors(),
    bodyParser.json(),    
    apolloUploadExpress(),
    graphqlExpress( { schema })
);

app.listen(4000);
console.log("start server!");